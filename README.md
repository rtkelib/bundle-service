# RTK bundle management library

This library can create, unpack, encrypt and decrypt bundles used in RTK projects.

Support creating from:
* Directory
* PDF-file
* ePub book
* ZIP-archive
* Another bundle (ZIP or binary)
* Any single file

Supported bundles:
* RTK binary bundles (with encryption support)
* RTK ZIP-bundles

## Installation

```sh
npm install git+ssh://git@projects.rtk-sdo.ru:rtk-ibc/core/bundle-service.git --save
```

## Usage


Service automatically resolves types of input and output by file extension or if passed path is a directory. So, supported extensions are:

* .epub - ePub book. Only read stream is supported
* .zip - ZIP-archives. Read and write streams are supported
* .aggregion - Old RTK ZIP-bundle. Read and write streams are supported
* .agb - RTK binary bundle. Read and write streams are supported
* .pdf (and any other single file) - Only read stream is supported

```javascript
const BundleService = require('agg-bundle-service');

// Create read stream

let readStream = BundleService.createReadStream({path: '/path/to/any/file/or/directory/example.epub'});

// Create write stream

let writeStream = BundleService.createWriteStream({path: '/path/to/any/file/example.agb', info: {someInfoToMix: 'info'}});

// Pipe one to another

readStream.pipe(writeStream);

// Waiting for finish

writeStream.on('finish', () => {
    console.log('Done!');
});

// Encrypt

let encryptor = BundleService.createEncryptor(new Buffer(16));

readStream
  .pipe(encryptor)
  .pipe(writeStream);

// Decrypt

let decryptor = BundleService.createDecryptor(new Buffer(16));

readStream
  .pipe(decryptor)
  .pipe(writeStream);

// Render

let rendererStream = BundleService.createRenderer({path: 'path/to/render.pdf', pages: [1, 2, '3-10']});

readStream
  .pipe(decryptor)
  .pipe(rendererStream);

// Get source bundle info

readStream.on('ready', () => {
    let info = readStream.getInfo();
    let props = readStream.getProps();
    let filesList = readStream.getFiles();
    let filesCount = readStream.getFilesCount();
});

```
## Console usage

### Print help

```sh
makebundle --help
```
### Make bundle

```sh
makebundle -i path/to/index/file -o /path/to/output /path/to/input/file/or/directory
```

## Run tests

```sh
npm test
```
